from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import setLogLevel
from mininet.cli import CLI
from mininet.node import OVSSwitch, Controller, RemoteController
from time import sleep


class MyTopo(Topo):
    "Multicontroller topology"
    def build(self):
        s0 = self.addSwitch('s0', failMode='secure')
        s1 = self.addSwitch('s1', failMode='secure')
        s2 = self.addSwitch('s2', failMode='secure')
        s3 = self.addSwitch('s3', failMode='secure')
        s4 = self.addSwitch('s4', failMode='secure')
        s5 = self.addSwitch('s5', failMode='secure')

        h1 = self.addHost('h1', mac="00:00:00:00:00:01", ip="10.0.0.1/24")
        h2 = self.addHost('h2', mac="00:00:00:00:00:02", ip="10.0.0.2/24")
        h3 = self.addHost('h3', mac="00:00:00:00:00:03", ip="10.0.0.3/24")
        h4 = self.addHost('h4', mac="00:00:00:00:00:04", ip="10.0.0.4/24")
        h5 = self.addHost('h5', mac="00:00:00:00:00:05", ip="10.0.0.5/24")
        h6 = self.addHost('h6', mac="00:00:00:00:00:06", ip="10.0.0.6/24")
        h7 = self.addHost('h7', mac="00:00:00:00:00:07", ip="10.0.0.7/24")
        h8 = self.addHost('h8', mac="00:00:00:00:00:08", ip="10.0.0.8/24")

        self.addLink(h1, s2)
        self.addLink(h2, s2)
        self.addLink(h3, s3)
        self.addLink(h4, s3)
        self.addLink(h5, s4)
        self.addLink(h6, s4)
        self.addLink(h7, s5)
        self.addLink(h8, s5)

        self.addLink(s0, s2)
        self.addLink(s0, s3)
        self.addLink(s0, s4)
        self.addLink(s0, s5)

if __name__ == '__main__':
    setLogLevel('info')
    topo = MyTopo()
    net = Mininet(topo=topo,  build=False)
    c0 = RemoteController('c0', ip='127.0.0.1', port=6653)
    c1 = RemoteController('c1', ip='127.0.0.1', port=6654)
    net.addController(c0)
    net.addController(c1)
    net.build()
    net.start()
    CLI(net)
    net.stop()
